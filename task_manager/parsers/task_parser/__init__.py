import logging
import sys
from collections import OrderedDict

import yaml

from utils import get_directory_filenames, merge_dicts

logging.basicConfig(level=logging.INFO, stream=sys.stdout)


class TasksParser:
    TEMPLATE_EXTENSIONS = ['yml', 'yaml']

    TASK_GLOBAL_OPTIONAL_KEYWORDS = ['variables']
    TASK_JOB_REQUIRED_KEYWORDS = []
    TASK_JOB_OPTIONAL_KEYWORDS = ['variables']

    def __init__(self):
        self._tasks = OrderedDict()

    @staticmethod
    def get_task_content(filepath):
        with open(filepath) as task_file:
            task_contents = task_file.read()

            try:
                return yaml.load(task_contents)
            except yaml.YAMLError as exc:
                logging.error(exc)
                return None

    @staticmethod
    def templatize_task(task_contents, variables):
        for var in variables:
            task_contents = task_contents.replace('$%s' % var, variables[var])\
                                         .replace('${%s}' % var, variables[var])

        return task_contents

    def normalize_task_content(self, task_path, content):
        task_name = content.get('name')

        if task_name is None:
            logging.error('"name" field of task is not specified: task = %s' % task_path)
            return None

        template_name = content.get('template')

        if template_name is None:
            logging.error('"template" field of task is not specified: task = %s' % task_path)
            return None

        task_jobs = content.get('jobs', {})

        result = {
            'name': task_name,
            'template': template_name,
            'variables': content.get('variables', {}),
            'jobs': {}
        }
        global_values = {}

        for kw in TasksParser.TASK_GLOBAL_OPTIONAL_KEYWORDS:
            val = content.get(kw)

            if val is not None:
                global_values[kw] = val

        for job_name, job in task_jobs.items():
            if job_name in result['jobs'].keys():
                logging.error('Duplicate job name: job_name = %s, task = %s' % (job_name, task_path))
                return None

            result['jobs'][job_name] = {}

            # parse required keywords

            for kw in TasksParser.TASK_JOB_REQUIRED_KEYWORDS:
                local_val = job.get(kw)
                global_val = global_values.get(kw)

                if local_val and global_val is None:
                    logging.error(
                        '"%s" field of the job is not specified: job_name = %s, task = %s' % (kw, job_name, task_path))
                    return None

                if local_val is None:
                    final_val = global_val
                elif global_val is None:
                    final_val = local_val
                else:
                    final_val = merge_dicts(global_val, local_val)

                result['jobs'][job_name][kw] = final_val

            # parse optional keywords

            for kw in TasksParser.TASK_JOB_OPTIONAL_KEYWORDS:
                local_val = job.get(kw)
                global_val = global_values.get(kw)

                final_val = None

                if local_val is not None and global_val is not None:
                    final_val = merge_dicts(global_val, local_val)
                elif local_val is not None:
                    final_val = local_val
                elif global_val is not None:
                    final_val = global_val

                if final_val is not None:
                    result['jobs'][job_name][kw] = final_val

        # sort jobs by name
        result['jobs'] = OrderedDict(sorted(result['jobs'].items()))

        return result

    def get_tasks(self):
        return self._tasks

    def load_tasks(self, tasks_paths):
        tasks = {}
        tasks_loaded = True

        try:
            for tasks_path in tasks_paths:
                tasks_files = get_directory_filenames(tasks_path, TasksParser.TEMPLATE_EXTENSIONS)

                for task_path in tasks_files:
                    content = self.get_task_content(task_path)

                    if content is None:
                        logging.error('Task content is not defined: task = "%s"' % task_path)
                        tasks_loaded = False
                        break

                    normalized_content = self.normalize_task_content(task_path, content)

                    if normalized_content is None:
                        logging.error('Task content can not be normalized: task = %s' % task_path)
                        tasks_loaded = False
                        break

                    tasks[normalized_content['name']] = normalized_content

                if not tasks_loaded:
                    break

        except Exception as e:
            logging.error('Tasks have not been loaded')
            tasks_loaded = False

        if tasks_loaded:
            # sort tasks by name
            tasks = OrderedDict(sorted(tasks.items()))

            self._tasks = tasks

        return tasks_loaded

    def list_tasks(self):
        logging.info('=' * 80)
        logging.info('Loaded %s tasks:\n' % len(self._tasks.keys()))

        for task_name, task in self._tasks.items():
            logging.info('* %s' % task_name)

            for job_name, job in task.get('jobs', {}).items():
                logging.info('  - %s' % job_name)

        logging.info('')
        logging.info('=' * 80)
