### Description:

RIP is a simple deployment tool that does your deployments on API calls.

### Usage:

Here is how you can run RIP:

`$ ripd -c /opt/rip/rip.yml -a 0.0.0.0 -p 5678`

This command will start RIP webhook on port 5678 on all network interfaces.
(Please, be sure to hide your RIP on from revealing to unwanted networks by providing an appropriate IP-address.)

`-c` start parameter allows you to point the main RIP configuration file.

### Configuration file sample:


```yaml
TASKS_PATHS:
  - /opt/rip/tasks/tasks_set_1
  - /opt/rip/tasks/tasks_set_2

TEMPLATES_PATHS:
  - /opt/rip/templates
```

Here:

- `TASKS_PATHS` is the listing of paths where task descriptors are stored
- `TEMPLATES_PATHS` is the listing of paths where task templates are stored


### Task template sample:


```yaml
name: 'deploy from git repository'

token: $TOKEN
working_dir: $WORKING_DIR


jobs:
    deploy:
        script:
            - supervisordctl stop $APP_NAME
            - rm -r ./$APP_NAME
            - git clone git@gitlab.com:username/$APP_NAME.git ./$APP_NAME
            - supervisorctl start $APP_NAME
```

Here:

- `token`_(global/local)_ is a secret key to be matched against the one from the API request payload. Only if they match the task will start execution
- `working_dir`_(global/local)_ is the Current Working Directory. All the commands within a `script`_(local)_ section are executed under this path.
- `jobs` is a sections where tasks are described
- `deploy` - name of your task


### Task sample:

```yaml
name: 'task_1'

template: 'deploy from git repository'

variables:
    TOKEN: 'task token'
    APP_NAME: 'application'

jobs:
    deploy:
        variables:
            WORKING_DIR: /srv/application
```

**Overriding rule:** locals are first priority. According global is only applied to the job if there is no such local.

### Installation

0. Edit `rip.service` file: replace all entries of `/opt/rip/` for the path you've put rip to, all the entries of `/opt/rip/config.yml` for the path to RIP's main configuration file, and `demo` for the name of the user you wish to start the service from
1. Copy `rip.service` file into `/etc/systemd/system/` folder
2. Type `systemctl enable rip` in order to activate the `rip` service
3. Type `systemctl start rip` in order to start the `rip` service

To check the status, type in: `systemctl status rip`.
To restart the service, type `systemctl restart rip`.


### Runtime configuration

- `$ rip-cli -rc` - reload RIP's main configuration file

- `$ rip-cli -rt` - reload tasks
