import argparse
import collections
import copy
import glob
import logging
import os
import sys

import yaml

logging.basicConfig(level=logging.INFO, stream=sys.stdout)


DEFAULT_SOCK_PATH = '/tmp/rip.sock'


def get_server_args():
    parser = argparse.ArgumentParser(description='[SERVER] Rest In Production(RIP) is a deployment tool, '
                                                 'that acts on REST API calls.')

    parser.add_argument('-c', '--config', help='Path to configuration file', dest='config_path',
                        default='/etc/rip/rip.yml', required=True)
    parser.add_argument('-a', '--address', help='API hook IP-address', required=False, default='localhost', dest='host')
    parser.add_argument('-p', '--port', help='API hook port', default='45232', required=False)
    parser.add_argument('-s', '--socket', help='Socket file path', dest='socket_path', default=DEFAULT_SOCK_PATH,
                        required=False)

    args = parser.parse_args()

    if not os.path.exists(args.config_path) or not os.path.isfile(args.config_path):
        raise Exception('Configuration file does not exist: "%s"' % args.config_path)

    task_templates_paths = ['/etc/rip/conf.d/']
    tasks_paths = []

    # load tasks sets directories list

    with open(args.config_path) as config_file:
        config_contents = yaml.load(config_file)

        if config_contents is None:
            raise Exception('Configuration file is not valid: "%s"' % args.config_path)

        task_templates_paths.extend(config_contents.get('TEMPLATES_PATHS', []))
        tasks_paths.extend(config_contents.get('TASKS_PATHS', []))

    setattr(args, 'templates_paths', task_templates_paths)
    setattr(args, 'tasks_paths', tasks_paths)

    return args


def parse_client_args():
    parser = argparse.ArgumentParser(description='[CLIENT] Rest In Production(RIP) is a deployment tool, '
                                                 'that acts on REST API calls.')

    parser.add_argument('-s', '--socket', help='Socket file path', dest='socket_path', default=DEFAULT_SOCK_PATH,
                        required=False)
    parser.add_argument('-rc', '--reload-config', help='Reload the configuration', dest='should_reload_config',
                        action='store_true', default=False)
    parser.add_argument('-rt', '--reload-tasks', help='Reload tasks list', dest='should_reload_tasks',
                        action='store_true', default=False)

    args = parser.parse_args()

    return args


def get_directory_filenames(path, extensions):
    result = []

    for extension in extensions:
        for filename in glob.iglob('{path}/**/*.{extension}'.format(path=path,
                                                                    extension=extension),
                                   recursive=True):
            result.append(filename)

    return result


def merge_dicts(dct, merge_dct, create_new=True):
    """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :param create_new: should we put the result of a marge into a new dict
    :return: None
    """
    result = dct

    if create_new:
        result = copy.deepcopy(dct)

    for k, v in merge_dct.items():
        if (k in result and isinstance(result[k], dict)
                and isinstance(merge_dct[k], collections.Mapping)):
            merge_dicts(result[k], merge_dct[k], False)
        else:
            result[k] = merge_dct[k]

    return result
