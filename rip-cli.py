import socket
import sys

import logging

from commands import Commands
from config import BUFFER_SIZE
from utils import parse_client_args


logging.basicConfig(stream=sys.stdout, level=logging.INFO)


client_args = parse_client_args()


def create_client_socket():
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

    sock.connect(client_args.socket_path)

    return sock


def main():
    try:
        sock = create_client_socket()
    except Exception as e:
        logging.error('Could not create a client socket')
        sys.exit(-1)

    try:
        if client_args.should_reload_config is True:
            msg = Commands.config_reload.encode('utf-8')
            sock.sendall(msg)

            data = sock.recv(BUFFER_SIZE)
            logging.info('RESPONSE FROM DAEMON: "%s"' % data)

        if client_args.should_reload_tasks is True:
            msg = Commands.tasks_reload.encode('utf-8')
            sock.sendall(msg)

            data = sock.recv(BUFFER_SIZE)
            logging.info('RESPONSE FROM DAEMON: "%s"' % data)

    except Exception as e:
        logging.error(e.args)
    finally:
        logging.debug('Closing the socket..')
        sock.close()


if __name__ == '__main__':
    main()

